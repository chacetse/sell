package com.chen.sell.dao;

import com.chen.sell.domain.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepositoryTest {
    @Autowired
    private ProductCategoryRepository categoryRepository;

    @Test
    public void findOneTest(){
//        List<ProductCategory> all = categoryRepository.findAll();
        Optional<ProductCategory> byId = categoryRepository.findById(1);
        ProductCategory productCategory = byId.get();
        System.out.println(productCategory);
    }

    @Test
    @Transactional
    public void saveOneTest(){
        ProductCategory productCategory = new ProductCategory("all you can eat", 6);
        productCategory.setCreateTime(new Date());
        productCategory.setUpdateTime(new Date());
        ProductCategory save = categoryRepository.save(productCategory);
        Assert.assertNotNull(save);
    }

    @Test
    public void updateOneTest(){
        Optional<ProductCategory> byId = categoryRepository.findById(2);
        ProductCategory productCategory = byId.get();
        productCategory.setCategoryType(34);
        categoryRepository.save(productCategory);
    }

    @Test
    public void findByCategoryTypeInTest(){
        List<Integer> categoryTypeIdList = Arrays.asList(1, 5, 34, 99);
        List<ProductCategory> byCategoryTypeIn = categoryRepository.findByCategoryTypeIn(categoryTypeIdList);
        Assert.assertNotEquals(0, byCategoryTypeIn.size());
    }

}