package com.chen.sell;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * author: xie
 * Date: 2018/10/26
 * Time: 13:33
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NewFeaturesTest {

    @Test
    public void streamTest() {
        // 新方法：
        List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
        double bill = costBeforeTax.stream().map((cost) -> cost + .12 * cost).reduce((sum, cost) -> sum + cost).get();
        System.out.println("Total : " + bill);
    }

    @Test
    public void listTest() {
        // Java 8之后：
        List features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        features.forEach(n -> System.out.println(n));

        // 使用Java 8的方法引用更方便，方法引用由::双冒号操作符标示，
        // 看起来像C++的作用域解析运算符
        features.forEach(System.out::println);
    }
    @Test
    public void lambdaTest1() {
//        func(()-> System.out.println("********************* --new feature test-- *********************"));

        func((e)-> System.out.println("********************* --new feature test-- *********************" + e));
    }

    private void func(NewFeaturesTestFunction newFeaturesTestFunction) {
//        newFeaturesTestFunction.test();

        int x = 1;
        newFeaturesTestFunction.test(x);
    }
}
