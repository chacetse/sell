package com.chen.sell;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * author: xie
 * Date: 2018/10/19
 * Time: 10:19
 */
@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
public class LoggerTest {

    @Test
    public void test1() {
        log.info("info...{}, {}", "1", "2");
        log.debug("debug ....");
        log.info("info ....");
        log.error("error ....");
    }
}
