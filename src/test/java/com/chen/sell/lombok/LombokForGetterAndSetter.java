package com.chen.sell.lombok;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.collections.map.MultiValueMap;

import java.util.Map;

/**
 * author: xie
 * Date: 2018/10/19
 * Time: 13:15
 */
@Data
@ToString
@EqualsAndHashCode
public class LombokForGetterAndSetter {
    private String name;
    private Integer id;
    private Map<String, Integer> otherMap = new MultiValueMap();

    public Map getMap() {
        return getOtherMap();
    }
}
