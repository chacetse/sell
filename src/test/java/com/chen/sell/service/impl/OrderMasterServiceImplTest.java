package com.chen.sell.service.impl;

import com.chen.sell.domain.OrderDetail;
import com.chen.sell.dto.OrderMasterDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderMasterServiceImplTest {

    @Autowired
    private OrderMasterServiceImpl orderMasterService;
    private final String BUYER_OPENID = "134343";
    private final String ORDER_ID = "1540823779914974551";

    @Test
    public void paid(){
        OrderMasterDTO orderMasterDTO = orderMasterService.findOne(ORDER_ID);
        orderMasterService.cancel(orderMasterDTO);
        log.info("********支付订单********  result = {}", orderMasterDTO);
    }

    @Test
    public void cancel(){
        OrderMasterDTO orderMasterDTO = orderMasterService.findOne(ORDER_ID);
        orderMasterService.cancel(orderMasterDTO);
        log.info("********取消订单********  result = {}", orderMasterDTO);
    }

    @Test
    public void findList(){
        PageRequest pageRequest = PageRequest.of(1, 2);
        Page<OrderMasterDTO> orderMasterDTOPage = orderMasterService.findList(BUYER_OPENID, pageRequest);
        log.info("********查询订单列表********  result = {}", orderMasterDTOPage);
    }

    @Test
    public void findOne(){
        OrderMasterDTO orderMasterDTO = orderMasterService.findOne(ORDER_ID);
        log.info("********查询订单********  result = {}", orderMasterDTO);
    }

    @Test
    public void create() {
        OrderMasterDTO orderMasterDTO = new OrderMasterDTO();
        orderMasterDTO.setBuyerName("lss");
        orderMasterDTO.setBuyerAddress("nj");
        orderMasterDTO.setBuyerPhone("123456322");
        orderMasterDTO.setBuyerOpenid(BUYER_OPENID);

        List<OrderDetail> orderDetailList = new ArrayList<>();
        OrderDetail o1 = new OrderDetail();
        o1.setProductId("2");
        o1.setProductQuantity(2);
        orderDetailList.add(o1);

        OrderDetail o2 = new OrderDetail();
        o2.setProductId("1");
        o2.setProductQuantity(5);
        orderDetailList.add(o2);

        orderMasterDTO.setOrderDetailList(orderDetailList);

        OrderMasterDTO result = orderMasterService.create(orderMasterDTO);
        log.info("********创建订单********  result = {}", result);
    }
}