package com.chen.sell;

/**
 * 函数接口：只有一个方法的接口。作为Lambda表达式的类型
 * author: xie
 * Date: 2018/10/26
 * Time: 13:39
 */
public interface NewFeaturesTestFunction {
//    void test();
    void test(int integer);

}
