package com.chen.sell.converter;

import com.chen.sell.domain.OrderMaster;
import com.chen.sell.dto.OrderMasterDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author xie
 * @Date 2018-10-27 20:15
 */
public class OrderMaster2OrderMasterDTOConverter {
    public static OrderMasterDTO convert(OrderMaster orderMaster) {
        OrderMasterDTO orderMasterDTO = new OrderMasterDTO();
        BeanUtils.copyProperties(orderMaster, orderMasterDTO);
        return orderMasterDTO;
    }

    public static List<OrderMasterDTO> convert(List<OrderMaster> orderMasterList) {
        return orderMasterList.stream().map(e -> convert(e)).collect(Collectors.toList());
    }
}
