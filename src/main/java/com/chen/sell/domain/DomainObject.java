package com.chen.sell.domain;


import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * author: xie
 * Date: 2018/10/23
 * Time: 9:33
 */
@MappedSuperclass
@Data
public class DomainObject {
    private Date createTime;
    private Date updateTime;
}
