package com.chen.sell.domain;

import com.chen.sell.enums.OrderStatusEnum;
import com.chen.sell.enums.PayStatusEnum;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * author: xie
 * Date: 2018/10/23
 * Time: 13:34
 */
@Entity
@Data
@DynamicUpdate
public class OrderMaster extends DomainObject {
    @Id
    private String orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerOpenid;
    private BigDecimal orderAmount;
    private Integer orderStatus = OrderStatusEnum.NEW.getCode();
    private Integer payStatus= PayStatusEnum.WAIT.getCode();
    private String buyerAddress;
    private Date createTime;
    private Date updateTime;

//    @Transient
//    private List<OrderDetail> orderDetailList;
}
