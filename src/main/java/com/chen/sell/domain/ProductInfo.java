package com.chen.sell.domain;

import com.chen.sell.enums.ProductStatusEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * author: xie
 * Date: 2018/10/23
 * Time: 13:28
 */
@Entity
@Data
public class ProductInfo extends DomainObject {
    @Id
    private String productId;
    private String productName;
    private BigDecimal productPrice;
    private Integer productStock;
    private String productIcon;
    private Integer categoryType;
    private String productDescription;
    /** 状态, 0正常1下架. */
    private Integer productStatus = ProductStatusEnum.UP.getCode();
}
