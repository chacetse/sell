package com.chen.sell.util;

import java.util.Random;

/**
 * @Author xie
 * @Date 2018-10-25 21:48
 */
public class KeyUtil {
    /**
     * 生成唯一的主将
     * 格式： 时间+随机数
     * @return
     */
    public static synchronized String genUniqueKey() {
        Random random = new Random();
        int randomNum = random.nextInt(900000) + 100000;
        return System.currentTimeMillis() + String.valueOf(randomNum);
    }
}
