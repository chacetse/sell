package com.chen.sell.enums;

import lombok.Getter;

/**
 * @Author xie
 * @Date 2018-10-25 21:06
 */
@Getter
public enum PayStatusEnum {
    WAIT(0, "未支付"),
    SUCCESS(1, "支付成功"),
    ;
    private Integer code;
    private String message;

    PayStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
