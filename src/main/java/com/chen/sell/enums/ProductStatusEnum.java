package com.chen.sell.enums;

import lombok.Getter;

/**
 * @Author xie
 * @Date 2018-10-23 22:44
 */
@Getter
public enum ProductStatusEnum {
    UP(0, "上架"),
    DOWN(1, "下架");
    private Integer code;
    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
