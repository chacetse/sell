package com.chen.sell.dto;

import com.chen.sell.domain.OrderDetail;
import com.chen.sell.serializer.Date2LongSerizlizer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author xie
 * @Date 2018-10-25 21:25
 */
@Data
//@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderMasterDTO {
    private String orderId;
    private String buyerName;
    private String buyerPhone;
    private String buyerOpenid;
    private BigDecimal orderAmount;
    private Integer orderStatus;
    private Integer payStatus;
    private String buyerAddress;
    @JsonSerialize(using = Date2LongSerizlizer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongSerizlizer.class)
    private Date updateTime;
    private List<OrderDetail> orderDetailList;
}
