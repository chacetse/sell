package com.chen.sell.dto;

import lombok.Data;

/**
 * @Author xie
 * @Date 2018-10-27 18:12
 */
@Data
public class CartDTO {
    private String productInfoId;
    private Integer productQuantity;

    public CartDTO(String productInfoId, Integer productQuantity) {
        this.productInfoId = productInfoId;
        this.productQuantity = productQuantity;
    }
}
