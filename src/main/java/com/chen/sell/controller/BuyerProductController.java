package com.chen.sell.controller;

import com.chen.sell.domain.ProductCategory;
import com.chen.sell.domain.ProductInfo;
import com.chen.sell.service.ProductCategoryService;
import com.chen.sell.service.ProductInfoService;
import com.chen.sell.util.ResultVOUtil;
import com.chen.sell.vo.ProductCategoryVO;
import com.chen.sell.vo.ProductInfoVO;
import com.chen.sell.vo.ResultVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author xie
 * @Date 2018-10-24 21:42
 */
@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
    @Autowired
    ProductInfoService productInfoService;
    @Autowired
    ProductCategoryService productCategoryService;

    @GetMapping("/list")
    public ResultVO<List<ProductCategoryVO>> list() {
        // 1、查询所有的上架商品
        List<ProductInfo> productInfoServiceUpAll = productInfoService.findUpAll();

        // 2、查询类目（一次查询）
//        List<Integer> categoryTypeList = new ArrayList<>();
        // 传统方法
//        for (ProductInfo productInfo : productInfoServiceUpAll) {
//            categoryTypeList.add(productInfo.getCategoryType());
//        }
        // 精简方法  Java8 lambda
        List<Integer> categoryTypeList = productInfoServiceUpAll.stream().map(e -> e.getCategoryType()).collect(Collectors.toList());
        List<ProductCategory> productCategoryList = productCategoryService.findByCategoryTypeIn(categoryTypeList);
        // 3、数据拼装
        List<ProductCategoryVO> productCategoryVOList = new ArrayList<>();
        for (ProductCategory productCategory : productCategoryList) {
            ProductCategoryVO productCategoryVO = new ProductCategoryVO();
            productCategoryVO.setCategoryName(productCategory.getCategoryName());
            productCategoryVO.setCategoryType(productCategory.getCategoryType());

            List<ProductInfoVO> productInfoVOList = new ArrayList<>();
            for (ProductInfo productInfo : productInfoServiceUpAll) {
                if (productInfo.getCategoryType() == productCategory.getCategoryType()) {
                    ProductInfoVO productInfoVO = new ProductInfoVO();
                    BeanUtils.copyProperties(productInfo, productInfoVO);
                    productInfoVOList.add(productInfoVO);
                }
            }
            productCategoryVO.setProductInfoVOList(productInfoVOList);
            productCategoryVOList.add(productCategoryVO);
        }

        return ResultVOUtil.success(productCategoryVOList);
    }

}
