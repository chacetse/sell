package com.chen.sell.service;

import com.chen.sell.domain.ProductInfo;
import com.chen.sell.dto.CartDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @Author xie
 * @Date 2018-10-23 22:39
 */
public interface ProductInfoService {
    ProductInfo findOne(String productId);

    /**
     * 查询所有上架商品列表
     * @return
     */
    List<ProductInfo> findUpAll();

    Page<ProductInfo> findAll(Pageable pageable);

    ProductInfo save(ProductInfo productInfo);

    // 加库存
    void increaseStock(List<CartDTO> cartDTOList);

    // 减库存
    void decreaseStock(List<CartDTO> cartDTOList);

}
