package com.chen.sell.service.impl;

import com.chen.sell.dto.OrderMasterDTO;
import com.chen.sell.enums.ResultEnum;
import com.chen.sell.exception.SellException;
import com.chen.sell.service.BuyerService;
import com.chen.sell.service.OrderMasterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author xie
 * @Date 2018-10-30 22:24
 */
@Service
@Slf4j
public class BuyerServiceImpl implements BuyerService {
    @Autowired
    private OrderMasterService orderMasterService;

    @Override
    public OrderMasterDTO findOrderOne(String openid, String orderId) {
        return checkOrderOwner(openid, orderId);
    }

    private OrderMasterDTO checkOrderOwner(String openid, String orderId) {
        OrderMasterDTO orderMasterDTO = orderMasterService.findOne(orderId);
        if (orderMasterDTO == null) return null;

        if (!orderMasterDTO.getBuyerOpenid().equalsIgnoreCase(openid)) {
            log.error("[查询订单] 订单的openid不一致， openid={}, orderMasterDTO={}", openid, orderMasterDTO);
            throw new SellException(ResultEnum.ORDER_OWNER_ERROR);
        }
        return orderMasterDTO;
    }

    @Override
    public OrderMasterDTO cancelOrder(String openid, String orderId) {
        OrderMasterDTO orderMasterDTO = checkOrderOwner(openid, orderId);
        if (orderMasterDTO == null) {
            log.error("[取消订单] 查不到该订单， orderId={}", orderId);
            throw new SellException(ResultEnum.ORDER_NOT_EXIST);
        }
        return orderMasterService.cancel(orderMasterDTO);
    }
}
