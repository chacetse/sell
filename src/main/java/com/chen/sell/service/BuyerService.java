package com.chen.sell.service;

import com.chen.sell.dto.OrderMasterDTO;

/**
 * @Author xie
 * @Date 2018-10-30 22:22
 */
public interface BuyerService {
    // 查询一个订单
    OrderMasterDTO findOrderOne(String openid, String orderId);


    // 取消订单
    OrderMasterDTO cancelOrder(String openid, String orderId);
}
