package com.chen.sell.service;

import com.chen.sell.domain.ProductCategory;

import java.util.List;

/**
 * @Author xie
 * @Date 2018-10-23 22:14
 */
public interface ProductCategoryService {
    ProductCategory findOne(Integer productCategoryId);

    List<ProductCategory> findAll();

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

    ProductCategory save(ProductCategory productCategory);
}
