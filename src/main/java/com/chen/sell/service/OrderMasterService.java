package com.chen.sell.service;

import com.chen.sell.domain.OrderMaster;
import com.chen.sell.dto.OrderMasterDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author xie
 * @Date 2018-10-25 21:22
 */

public interface OrderMasterService {
    /**
     * 创建订单
     */
    OrderMasterDTO create(OrderMasterDTO orderMasterDTO);

    /**
     * 查询单个订单
     */
    OrderMasterDTO findOne(String orderId);

    /**
     * 查询订单列表
     */
    Page<OrderMasterDTO> findList(String buyerOpenid, Pageable pageable);

    /**
     * 取消订单
     */
    OrderMasterDTO cancel(OrderMasterDTO orderMasterDTO);

    /**
     *完结订单
     */
    OrderMasterDTO finish(OrderMasterDTO orderMasterDTO);

    /**
     * 支付订单
     */
    OrderMasterDTO paid(OrderMasterDTO orderMasterDTO);
}
