package com.chen.sell.exception;

import com.chen.sell.enums.ResultEnum;

/**
 * @Author xie
 * @Date 2018-10-25 21:35
 */
public class SellException extends RuntimeException {
    private Integer code;

    public SellException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public SellException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
